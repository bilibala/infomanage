package com.infomanager.model;

/**
 * Created by Jin on 2016/3/9.
 */
public class FileModel {
	  public String thumbnail;
    public String mimeType;
    public String filePath; //文件路径
    public String title;
    public String year;
    public String size;
    public Boolean selected; //是否被选
    public Boolean onCloud; //是否在云端
}
