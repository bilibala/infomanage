'use strict'
import React, {
    Image,
    View,
    Text,
    Component,
    StyleSheet,
    TouchableOpacity,
    ToastAndroid,
} from 'react-native';


var GroupInfo = require('./GroupInfo');
class GroupItem extends Component {
    
    render() {

        var image = null;
        if (this.props.story.icon) {
            image = <Image
                source={require('./image/icon_drawer_head.jpg')}
                style={styles.headIcon}
            />
        }

        return (
            // <View {...this.props}>
            <TouchableOpacity
                onPress={()=>this.props.clickItem(this.props.story.id)}>
                <View style={styles.row}>
                    {image}
                    <View style={styles.textColumn}>
                        <Text numberOfLines={1} style={styles.textTitle}>
                            {this.props.story.title}
                        </Text>

                        <Text style={styles.textMessage}>
                            {this.props.story.lastMessage}
                        </Text>
                    </View>
                    <View style={styles.timeColumn}>
                        <Text>
                            {this.props.story.time}
                        </Text>
                        <Image style={styles.notifyIcon} source={require('./image/notify_close.png')}/>
                    </View>
                </View>
            </TouchableOpacity>
            // </View>
        )
    }
}


var styles = StyleSheet.create({
    textTitle: {
        flex: 1,
        fontSize: 18,
        color: '#333333',
    },

    textMessage: {
        flex: 1,
        fontSize: 12,
    },

    row: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',  //在当灵活容器内的各项没有占用主轴上所有可用的空间时对齐容器内的各项（水平）
        backgroundColor: 'white',
        padding: 10,
    },

    headIcon: {
        height: 56,
        marginLeft: 10,
        marginRight: 10,
        width: 56,
        borderRadius: 28,
    },
    textColumn: {
        flex: 1,
    },
    timeColumn: {
        alignItems: 'flex-end'
    },

    notifyIcon: {
        height: 20,
        width: 20,
    }
});

module.exports = GroupItem;