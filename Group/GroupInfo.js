"use strict";

import React, {
    View,
    Component,
    StyleSheet,
    Image,
    Text,
    Switch,
    ScrollView,
    ToolbarAndroid,
} from 'react-native'


import GroupMemberGrid from './GroupMemberGrid';

class GroupInfo extends Component {


    constructor() {
        super();
        this.state = {
            notify: false,
            chatTop: false,
        };
    }


    render() {
        return (
            <View style={styles.container}>
                <ToolbarAndroid
                    navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha')}
                    title="群组信息"
                    style={styles.toolbar}
                    onIconClicked={this.props.navigator.pop}
                />
                <ScrollView >
                    <View style={styles.all_member}>
                        <Text style={styles.itemText}>全部成员</Text>
                        <GroupMemberGrid/>
                    </View>
                    <View style={styles.info_item}>
                        <View style={[styles.left,styles.item_member]}>
                            <Text style={styles.itemText}>群聊名称</Text>
                        </View>
                        <View style={[styles.right,styles.item_member]}>
                            <Text style={styles.itemText}>未命名</Text>
                        </View>
                    </View>
                    <View style={styles.info_item}>
                        <View style={[styles.left,styles.item_member]}>
                            <Text style={styles.itemText}>群二维码</Text>
                        </View>
                        <View style={[styles.right,styles.item_member]}>
                            <Image source={require('./image/qrcode.png')} style={styles.itemIcon}/>
                        </View>
                    </View>
                    <View style={styles.info_item}>
                        <View style={[styles.left,styles.item_member]}>
                            <Text style={styles.itemText}>消息免打扰</Text>
                        </View>
                        <View style={[styles.right,styles.itemSwitch]}>
                            <Switch
                                disabled={false}
                                onValueChange={this.notifyChange.bind(this)}
                                value={this.state.notify}>
                            </Switch>
                        </View>
                    </View>
                    <View style={styles.info_item}>
                        <View style={[styles.left,styles.item_member]}>
                            <Text style={styles.itemText}>聊天置顶</Text>
                        </View>
                        <View style={[styles.right,styles.itemSwitch]}>
                            <Switch
                                disabled={false}
                                onValueChange={this.setChatTop.bind(this)}
                                value={this.state.chatTop}/>
                        </View>
                    </View>
                    <View style={styles.info_item}>
                        <View style={[styles.left,styles.item_member]}>
                            <Text style={styles.itemText}>查找聊天内容</Text>
                        </View>
                    </View>
                    <View style={styles.info_item}>
                        <View style={[styles.left,styles.item_member]}>
                            <Text style={styles.itemText}>清理聊天记录</Text>
                        </View>
                    </View>

                    <View style={styles.info_item}>
                        <View style={[styles.center,styles.item_member]}>
                            <Text style={styles.btnText}>删除并退出</Text>
                        </View>
                    </View>
                    <View style={styles.info_item}></View>
                </ScrollView>
            </View>
        );
    }


    notifyChange(value) {
        this.setState({
            notify: value,
        });
    }

    setChatTop(value) {
        this.setState({
            chatTop: value,
        });
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f1f1f1',
    },
    toolbar: {
        backgroundColor: '#6d9eeb',
        height: 56,
    },
    all_member: {
        backgroundColor: '#fff',
        padding: 8,
    },
    info_item: {
        height: 56,
        flexDirection: 'row',
    },
    item_member: {
        justifyContent: 'center',

    },
    left: {
        flex: 1,
        marginLeft: 12,
    },
    right: {
        width: 80,
    },
    center: {
        flex: 1,
        marginLeft: 60,
        marginRight: 60,
        backgroundColor: '#f00',
        borderRadius: 8,
        alignItems: 'center',
        marginTop: 8,
        marginBottom: 8,
    },
    itemText: {
        fontSize: 18,
    },
    itemIcon: {
        height: 40,
        width: 40,
        margin: 8,
    },
    itemSwitch: {
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    btnText: {
        color: '#fff',
    }
});

export default GroupInfo;