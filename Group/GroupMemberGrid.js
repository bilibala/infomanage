"use strict";

import React, {
    View,
    Component,
    StyleSheet,
    Image,
    ListView,
} from 'react-native'


import Loader from '../component/loader/Loader';

var members = [
    {
        headIcon: '/image/icon_drawer_head.jpg',
        id: 1,
    },
    {
        headIcon: '/image/icon_drawer_head.jpg',
        id: 2,
    },
    {
        headIcon: '/image/icon_drawer_head.jpg',
        id: 3,
    },
    {
        headIcon: '/image/icon_drawer_head.jpg',
        id: 4,
    },
    {
        headIcon: '/image/icon_drawer_head.jpg',
        id: 5,
    },
    {
        headIcon: '/image/icon_drawer_head.jpg',
        id: 5,
    },
    {
        headIcon: '/image/icon_drawer_head.jpg',
        id: 5,
    },
    {
        headIcon: '/image/icon_drawer_head.jpg',
        id: 5,
    },
]

var groupMember = [];
class GroupMemberGrid extends Component {


    constructor() {
        super();
        this.state = {
            isLoaded: false,
            dataSource: null,
        };
    }

    componentDidMount() {
        //加载数据 members

        //
        // members.push({
        //     headIcon: '../Iftt/image/IFTTT/add.png',
        //     id: '-1',
        // });
        //
        // if (this.state.isHost) {
        //     members.push({
        //         headIcon: './image/minus.png',
        //         id: '-2',
        //     });
        // }

        groupMember.length = 0;
        var j = 0;
        var itemMembers = [];
        for (var i = 0; i < members.length; i++) {
            if (j == 4) {
                j = 0;
                groupMember.push(itemMembers);
                itemMembers.length = 0;
            }
            itemMembers.push(members[i]);
            j++;
        }


        var ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.setState({
            isLoaded: true,
            dataSource: ds.cloneWithRows(groupMember),
        });
    }


    render() {
        if (this.state.isLoaded) {
            return (
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={this.renderRow.bind(this)}
                />);

        }

        return (
            <View style={{alignItems:'center',}}>
                <Loader
                    size={37}
                    isVisible={true}
                    type={'ThreeBounce'}
                    color={'#6d9eeb'}
                />
            </View>

        );

    }


    renderRow(rowData) {
        return (
            <View style={styles.rowContainer}>
                <Image style={styles.headIcon} source={require('./image/icon_drawer_head.jpg')}/>
                <Image style={styles.headIcon} source={require('./image/icon_drawer_head.jpg')}/>
                <Image style={styles.headIcon} source={require('./image/icon_drawer_head.jpg')}/>
                <Image style={styles.headIcon} source={require('./image/icon_drawer_head.jpg')}/>
            </View>
        );
    }
}

var styles = StyleSheet.create({
    rowContainer: {
        flex: 1,
        height: 60,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    headIcon: {
        height: 56,
        width: 56,
    },
})

export default GroupMemberGrid;