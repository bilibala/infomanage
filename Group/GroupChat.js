"use strict";

import React, {
    View,
    ToolbarAndroid,
    Component,
    Text,
    Image,
    StyleSheet,
    TextInput,
    ListView,
    TouchableOpacity,
} from 'react-native'
import GroupInfo from './GroupInfo';

var chats = [
    {
        headIcon: '',
        messageType: 0,
        message: 'who am I?',
        receive: false,
    },
    {
        headIcon: '',
        messageType: 0,
        message: 'who am I?',
        receive: false,
    },
    {
        headIcon: '',
        messageType: 0,
        message: 'who am I?',
        receive: true,
    },
    {
        headIcon: '',
        messageType: 1,
        message: 'who am I?',
        receive: false,
    },
    {
        headIcon: '',
        messageType: 0,
        message: 'The cloud stood humby in a corner of the sky. The morning crowned it eith splendor.',
        receive: false,
    },
    {
        headIcon: '',
        message: 'The cloud stood humby in a corner of the sky. The morning crowned it eith splendor.',
        receive: true,
    }
]

class GroupChat extends Component {

    constructor() {
        super();

        var ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.state = {
            search: "",
            dataSource: ds.cloneWithRows(chats),
            isLoader: true,
            extra: 0,
        }
    }


    render() {
        return (
            <View style={styles.container}>
                <ToolbarAndroid
                    navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha')}
                    title="群组名称"
                    style={styles.toolbar}
                    onIconClicked={this.props.navigator.pop}
                    actions={[{title: 'members',icon: require('./image/ic_group_white_24dp_2x.png'), show: 'always'}]}
                    onActionSelected={this._press.bind(this)}
                />
                <View style={styles.seatchBg}>
                    <TextInput
                        style={styles.searchInput}
                        onChangeText={this._searchText.bind(this)}
                        placeholder="搜索"
                    />
                </View>
                <View
                    style={styles.listChat}>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this.renderRow.bind(this)}/>
                </View>
                <View
                    style={styles.chatInputRow}>
                    <TouchableOpacity
                        onPress={this._btnExtra.bind(this,1)}>
                        <Image style={styles.inputBtn} source={require('./image/icon_microphone.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this._btnExtra.bind(this,2)}>
                        <Image style={styles.inputBtn} source={require('./image/icon_add.png')}/>
                    </TouchableOpacity>
                    <TextInput
                        style={styles.chatInput}
                        onChangeText={(text)=>{this.setState({message:text})}}
                        value={this.state.message}
                    />
                    <TouchableOpacity
                        onPress={this._sendMessage.bind(this)}>
                        <View style={styles.btnSend}>
                            <Text>发送</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                {this.renderExtra(this.state.extra)}
            </View>
        );
    }

    _press(position) {

        if (this.props.navigator) {
            this.props.navigator.push({
                name: 'GroupInfo',
                component: GroupInfo,
            });
        }
    }

    _searchText(text) {
        this.setState({
            search: text,
        });
    }

    renderRow(rowData) {
        if (rowData.receive) {

            return (
                <View style={[styles.messageRow,styles.messageRowLeft]}>
                    <Image style={styles.chatHeadIcon} source={require('./image/icon_drawer_head.jpg')}/>
                    <View style={{flex:1,flexDirection:'column'}}>

                        <View style={[styles.messageBubbleLeft,styles.messageBubble]}>
                            {this.renderMessageBubble(rowData.messageType, rowData.message)}
                        </View>

                    </View>
                </View>
            );
        }

        return (
            <View style={[styles.messageRow,styles.messageRowRight]}>
                <View style={{flex:1,flexDirection:'column'}}>
                    <View style={[styles.messageBubbleRight,styles.messageBubble]}>
                        {this.renderMessageBubble(rowData.messageType, rowData.message)}
                    </View>
                </View>
                <Image style={styles.chatHeadIcon} source={require('./image/icon_drawer_head.jpg')}/>
            </View>
        );
    }

    renderMessageBubble(typle, data) {
        if (typle == 1) {
            return (
                <Image style={styles.messageImage} source={require('./image/test_image_message.jpg')}/>
            );
        }

        return (
            <Text>{data}</Text>
        );
    }

    _sendMessage() {

        this.setState({
            message: "",
        });
    }

    _btnExtra(i) {

        this.setState({
            extra: this.state.extra == i ? 0 : i
        });
    }

    renderExtra(index) {

        var extraView;

        switch (index) {
            case 1:
                extraView = (
                    <View style={[{height:56}]}>
                        <Text>microphone</Text>
                    </View>
                );
                break;
            case 2:
                extraView = (
                    <View style={[{height:56}]}>
                        <Text>add</Text>
                    </View>
                );
                break;
            default:
                extraView = (
                    <View style={{height:0}}/>
                );
        }

        return extraView;
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    toolbar: {
        backgroundColor: '#6d9eeb',
        height: 56,
    },
    seatchBg: {
        height: 48,
        backgroundColor: '#ececec',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
    },
    searchInput: {
        height: 40,
        shadowColor: '#333',
        backgroundColor: '#fff',
        shadowRadius: 4,
    },
    listChat: {
        flex: 1,
        backgroundColor: '#f4f4f4',
    },
    chatInputRow: {
        height: 56,
        padding: 8,
        alignItems: 'center',
        flexDirection: 'row',
    },
    inputBtn: {
        height: 28,
        width: 28,
        margin: 4,
    },
    chatInput: {
        height: 40,
        flex: 1,
        backgroundColor: '#fff'
    },
    btnSend: {
        height: 32,
        width: 48,
        backgroundColor: "#109C58",
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
    },


    messageRow: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 16,
        alignItems: 'flex-start',
    },

    messageRowLeft: {
        justifyContent: 'flex-start',
    },
    messageRowRight: {
        justifyContent: 'flex-end',
    },
    chatHeadIcon: {
        width: 40,
        height: 40,
        marginLeft: 8,
        marginRight: 8,
    },

    messageBubble: {
        padding: 8,
        borderRadius: 8,
        flexDirection: 'column',
    },
    messageBubbleLeft: {
        backgroundColor: '#fff',
        marginRight: 80,
    },
    messageBubbleRight: {
        backgroundColor: '#37C137',
        marginLeft: 80,
    },
    messageText: {
        fontSize: 16,
    },
    messageImage: {
        width: 200,
        height: 200,
        resizeMode: 'contain',
    }
});

export default GroupChat;