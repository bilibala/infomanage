import * as types from './actionTypes';

function setNavigator(navigator){
	return {
		type : types.InitNavigator,
		navigator
	}
}

module.exports = {setNavigator}