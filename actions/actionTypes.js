// login
export const Login = "LOIIN"
export const LoginSuccess= "LOGIN_Success"
export const LoginFail="Login_Fail"
export const InitNavigator = "Init_Navigator"
export const UpdateTicket="UpdateTicket"

// serach
export const Search = "Search"
export const SearchCompelete = "SearchCompelete"

// tag
export const FetchTag = "FetchTag"
export const FetchTagSuccess = "FetchTagSuccess"
export const AddTag = "AddTag"
export const FetchFileByTag = "FetchFileByTag"
export const RemoveTag = "RemoveTag"


//deviceControl
export const OpenWeb = "Open_Web"
export const CloseWeb = "Close_Web"
