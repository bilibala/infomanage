"use strict";

import React, {
    View,
    Component,
    StyleSheet,
    Image,
    ListView,
    TouchableOpacity
} from 'react-native'
import RNFileSearch from '../File/RNFileSearch';
import getStorage from './storage.js';
import {FileIcon} from './utils/FileUtil';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux'

class TimeCell extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data:this.props.data
        };

    }

    componentDidMount() {

    }


    render() {
        var cells = [];
        for(var i = 0;i < this.props.count;i++){
            var index = i;
            cells.push(
                <View key={i} style={styles.imageContainer}>
                    {this.renderCell(i)}
                </View>
                );
        }
        return (
            <View style={styles.rowContainer}>
                {cells}
            </View>
        );
    }
    renderCell(index){
      let webdavpath = this.state.data[index].webdavpath;
      const {ticket} = this.props;
      let icon = FileIcon(webdavpath,ticket);
      return (
        <TouchableOpacity onPress={()=>this.pressCell(this.state.data[index])}>
            <Image
                style={styles.headIcon}
                source={icon}
            />
        </TouchableOpacity>
      )
    }
    pressCell(data){
        getStorage().getTicket().then( ret => {
            var ticket = this.props.ticket;
            var url = "http://42.159.143.17:6080/alfresco/webdav"+data.webdavpath+"?ticket="+ticket;
            RNFileSearch.openOnlineFile(encodeURI(url),data.name);
      }).catch( err => {
      });
    }
}

var styles = StyleSheet.create({
    rowContainer: {
        flex: 1,
        height: 90,
        flexDirection: 'row',
    },
    imageContainer:{
        height: 90,
        width: 90,
        alignItems: 'center',
        justifyContent: 'center'
    },
    headIcon: {
        height: 87,
        width: 87,
    },
})

function mapStateToProps(store){
    return {
      ticket : store.Login.ticket
    };
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({},dispatch);
}

export default connect (mapStateToProps,mapDispatchToProps)(TimeCell);
