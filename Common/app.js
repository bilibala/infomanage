'use strict'

 import React from 'react';
 import  { Component } from 'react-native';
import { createStore, applyMiddleware, combineReducers,compose} from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
// import devTools from 'remote-redux-devtools';
import Infomanager from './root'
import * as reducers from '../reducers';
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const reducer = combineReducers(reducers);
const store = createStoreWithMiddleware(reducer);

// const store = createStore(reducer);
//   , {}, compose(
//     applyMiddleware(thunk),
//     devTools()));
// if (module.hot) {
//     // Enable hot module replacement for reducers
//     module.hot.accept(() => {
//       const nextRootReducer = require('../reducers/index').default;
//       store.replaceReducer(nextRootReducer);
//     });
//  }



export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Infomanager />
      </Provider>
    );
  }
}
