'use strict';
import React, {
  
  Component,
  StyleSheet,
  ToastAndroid,
  Text,
  View,
  Image,
  ListView,
  TouchableOpacity,
  ToolbarAndroid,
  TextInput
} from 'react-native';

var toolbarActions =[
 
  
  {title: '登录', show: 'always'},
  
  
];

class Register extends Component{
    
     constructor(props) {
        super(props);

       this.state ={
       	 email:'',
       	 password:''
       }
         global.storage.load({
    key: 'loginState',

    //autoSync(默认为true)意味着在没有找到数据或数据过期时自动调用相应的同步方法
    autoSync: false,

    //syncInBackground(默认为true)意味着如果数据过期，
    //在调用同步方法的同时先返回已经过期的数据。
    //设置为false的话，则始终强制返回同步方法提供的最新数据(当然会需要更多等待时间)。
    syncInBackground: true
  }).then( ret => {
    //如果找到数据，则在then方法中返回
    alert(ret.name);
  }).catch( err => {
    //如果没有找到数据且没有同步方法，
    //或者有其他异常，则在catch中返回
    console.warn(err);
  })
       
      
   }




	    render(){ 

	    	return( 
          <View style={styles.main}>
	    		<ToolbarAndroid
                       navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha') }
                       title = {'注册'}
                       actions={toolbarActions}
                       style={styles.toolbar}
                        onIconClicked={this.onIconClicked.bind(this)}
                        onActionSelected={this.onActionSelected.bind(this)}
                       >
                           
                            
                       </ToolbarAndroid>

                <View style={styles.middle}>
                    <View style={styles.email} >
                       <Text  style={{fontSize:18,marginTop:12,height:25,marginLeft:12}}>
                        手机号
                       </Text>

                       <TextInput
                               
                               style={{height:50,width:230,fontSize:18}}
                               autoFocus ={true}
                               value={this.state.email}
                               onChangeText={this.onChangeText.bind(this)}
                             />
                    </View>

                    <View style={styles.password}>
                        <Text  style={{fontSize:18,marginTop:12,height:25,marginLeft:12}}>
                        验证码
                       </Text>

                       <TextInput
                               
                               style={{marginLeft:18,height:50,width:230,fontSize:18}}
                               
                               value={this.state.email}
                               onChangeText={this.onChangeText.bind(this)}
                             />
                    </View>

                    <View style={styles.password}>
                        <Text  style={{fontSize:18,marginTop:12,height:25,marginLeft:12}}>
                        密码
                       </Text>

                       <TextInput
                               
                               style={{marginLeft:18,height:50,width:230,fontSize:18}}
                               placeholder="(6至16位数字或字母)"
                               value={this.state.email}
                               onChangeText={this.onChangeText.bind(this)}
                             />
                    </View>

                    <View style={styles.loginbutton}>
                          <Text style={styles.buttonText}>注册</Text>
                    </View>


                </View>


                <View style={styles.bottom}>
		                 <View  style={styles.space}>

		                 </View>
		                 <View  style={styles.qqorchat} >
                            <Image
                              style={{height:60,width:60}}
                              source={require('./image/btn_qq_login.png')} />
                            <Image
                              style={{height:60,width:60,marginLeft:60}}
                              source={require('./image/btn_weixin_login.png')} />
		                 </View>

                 </View>

          </View>


                       )


	    }


	    onIconClicked(){
	    	this.props.navigator.pop()
	    }
	    onActionSelected(){
	    	alert('注册')
	    }

	    onChangeText(text){
    
		    this.setState({
		        email:text ,
		    });

}

}

const styles = StyleSheet.create({
  toolbar: {
    backgroundColor: '#6d9eeb',
    height: 56,
  },
  main:{
  	flex:1,
  	flexDirection:'column'
  },
  middle:{
  	 alignItems: 'center',
  	 
  	flexDirection:'column',
  	height:250,
  	marginTop:100
  },

  email:{
  	flex:1,
    
  	flexDirection:'row',
  },
  password:{
  	flex:1,
    marginTop:30,
  	flexDirection:'row',
  	
  },
  loginbutton:{
  	borderRadius:5,
  	height:42,
  	width:280,
  	backgroundColor:'#32B16C',
  	 marginLeft:10,
  	 marginTop:50,
  	 alignItems: 'center',
  	 justifyContent: 'center',

  },
  buttonText:{

    color:'#ffffff',
    fontSize:18
  },
  forget:{
  	 marginTop:25
  },
  bottom:{
    flex:1,
    
  	flexDirection:'column',
  	marginTop:30
  },
  space:{
       height:1,
       backgroundColor:'#41B777',
       marginLeft:15,
       marginRight:15
  },
  qqorchat:{
      marginTop:30,
      justifyContent:'center',
      flexDirection:'row'
  }

  });

export default Register