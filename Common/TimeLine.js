'use strict';

import React, {

	Component,
	StyleSheet,
	Text,
	View,
	Image,
	ListView,
	TouchableOpacity,
  ToolbarAndroid,
	Dimensions
}
from 'react-native';


import {formatDate} from './utils/TimeUtil'
import Loader from '../component/loader/Loader';
import TimeCellContainer from './TimeCellContainer'

class FriendList extends Component{

    constructor(props){
    	super(props);
    	this.state = this.getState();
    }

    getState(){
        var getSectionData = (dataBlob, sectionID) => {
            return dataBlob[sectionID];
        }

        var getRowData = (dataBlob, sectionID, rowID) => {
            return dataBlob[sectionID + ':' + rowID];
        }

        return {
            isload : false,
            dataSource : new ListView.DataSource({
                getSectionData          : getSectionData,
                getRowData              : getRowData,
                rowHasChanged           : (row1, row2) => row1 !== row2,
                sectionHeaderHasChanged : (s1, s2) => s1 !== s2
            })
        }
    }

    componentDidMount(){
      var data = this.adapterData(this.props.data);
      this.setState({
            isload:true,
          dataSource: data,
      });
    }


    adapterData(list){

      var data = {}
      var rowIDs =[];
      var sectionIDs =[];

      for(var i = 0;i < list.length;i++){
        var now=new Date(parseInt(list[i].updated));
        var year=now.getYear()+1900;
        var month=(now.getMonth()+1)<10?'0'+(now.getMonth()+1) : now.getMonth()+1;
        var date=now.getDate()<10 ? '0'+now.getDate() : now.getDate();
        var hour=now.getHours()<10? '0'+now.getHours() : now.getHours();
        var minute=now.getMinutes()<10?'0'+now.getMinutes() : now.getMinutes();
        var second=now.getSeconds()<10?'0'+now.getSeconds() : now.getSeconds();
        var sectionText = year+'-'+month+'-'+date;
        var rowText = sectionText+'-'+hour+':'+minute;

        var dayNames = new Array("星期日","星期一","星期二","星期三","星期四","星期五","星期六");
        var sectionDay = dayNames[now.getDay()];
        var sectiondata = {
            sectionText:sectionText,
            sectionDay:sectionDay
        }
        if(sectionIDs.indexOf(sectionText) == -1){

            sectionIDs.push(sectionText);
            data[sectionText] = sectiondata;
        }
        if(!rowIDs[sectionIDs.indexOf(sectionText)]){
                rowIDs[sectionIDs.indexOf(sectionText)] = [];
        }
        if(rowIDs[sectionIDs.indexOf(sectionText)].indexOf(rowText) == -1){
            rowIDs[sectionIDs.indexOf(sectionText)].push(rowText);
        }
        if(!data[sectionText+':'+rowText]){
            data[sectionText+':'+rowText] = [];
        }
        data[sectionText+':'+rowText].push(list[i]);

      }
      return this.state.dataSource.cloneWithRowsAndSections(data,sectionIDs,rowIDs);
    }


    renderSectionHeader(sectionData, sectionID) {
      return (
        <View style={styles.section}>
            <View style={styles.color}/>
            <Text style={styles.sectionText}>{sectionData.sectionText}</Text>
            <Text style={styles.sectionDay}>{sectionData.sectionDay}</Text>
        </View>
      )
    }

    renderRow(fileList,sectionID, rowID){

      console.log('fileList:' + fileList)
      return(
             <View  style ={styles.item}>
                <TimeCellContainer time={rowID.substring(11)} fileList={fileList}></TimeCellContainer>
             </View>
        );
    }

    render(){
      	if (!this.state.isload) {
            return(
            	<View style={{alignItems:'center'}}>
	               <Loader
    	               size={37}
    	               isVisible ={true}
                        type={'ThreeBounce'}
    	               color={'#6d9eeb'}
    	           />
	           </View>
            );
      	}
           else{
           return (

             <View style={styles.container}>
             <ToolbarAndroid
                       navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha')}
                       title = {'时间线'}
                       style={styles.toolbar}
                       onIconClicked={this.props.navigator.pop}
                       >
                       </ToolbarAndroid>
                       <View style={styles.container}>
                          <ListView
                            dataSource={this.state.dataSource}
                            renderRow={this.renderRow.bind(this)}
                            renderSectionHeader={this.renderSectionHeader.bind(this)}
                           />
                       </View>
             </View>
           	);
          }
      }
}


const styles = StyleSheet.create({

  container:{
    flex:1,
  },
    toolbar: {
    backgroundColor: '#6d9eeb',
    height: 56,
  },
  section:{
    flex:1,
    flexDirection:'row',
    alignItems: 'center',
  },
  color:{
    backgroundColor: '#b6b6b6',
    width: 18,
    height: 18,
    borderRadius: 9,
    marginLeft:5,
  },
  sectionText:{

    marginLeft:5,
    marginRight:20,
    fontSize:18,
    color:'#272822'
  },
  sectionDay:{

    marginLeft:0,
    marginRight:20,
    fontSize:16,
    color:'#5a5a5a'
  },
  icon:{

    height:37,
    width:37,
    borderRadius:20,
    marginLeft:25
  },
  item:{
    flex:1,
    justifyContent:'center'
  },
  friendName:{
    fontSize:16,
    marginLeft:20,
    color:'#273237'
  },
  middle:{
    justifyContent: 'center',
  },
  image:{
    justifyContent: 'center',
  }
});


export default FriendList
