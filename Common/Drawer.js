'use strict';
import React, {

  Component,
  StyleSheet,
  ToastAndroid,
  Text,
  View,
  Image,
  ListView,
  TouchableOpacity,Dimensions,BackAndroid
} from 'react-native';

 var width = Dimensions.get('window').width-56; //full width
import Login from './Login';

 var _navigator =null;

import FriendList from './Friend'
import TimeLine from './TimeLine'
import UserInfo from './UserInfo'
import SearchScreen from './SearchScreen'
// BackAndroid.addEventListener('hardwareBackPress', function() {
//      if (_navigator.getCurrentRoutes().length === 1) {
//        return false;
//      }
//        _navigator.pop();
//      return true;
// });

import getStorage from './storage';
import Tag from '../File/Tag';
var storage = getStorage().getInstance();
var DialogAndroid = require('react-native-dialogs');


class Drawer extends Component{
	 constructor(props) {
        super(props);
         var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state={
        	navigator :this.props.navigator,
          drawer:this.props.drawer,

          dataSource: ds.cloneWithRows([
           {
           src: require('./image/friend.png'),
            "title": "我的好友",

          },
          {
         src: require('./image/controlpanel.png'),
            "title": "控制面板",

          },{
           src: require('./image/tag.png'),
            "title": "标签",

          },{
          src: require('./image/search.png'),
            "title": "搜索",

          },
          {
           src: require('./image/setting.png'),
            "title": "系统设置",

          },{
           src: require('./image/advice.png'),
            "title": "反馈意见",

          },
            ])
        };
    }






           render(){


            _navigator = this.state.navigator;
            var options = {
              title: '提示',
              content: '退出登录',
              positiveText: '确认退出',
              negativeText: '取消',
              "onPositive": () =>{  _navigator.resetTo({
              name: 'loginout',
              component: Login,
              params:{
                navigator:_navigator,
              }})
              storage.remove({
                key: 'ticket'
              });},
              "onNegative": () => this.state.drawer(),
            };
           	  return(

                 <View style={styles.main}>


                   <Image source={require('./image/icon_drawer_head.jpg')} style={styles.head}>
              			  <View style={{flex:1,flexDirection:'row'}}>
              			  	      <TouchableOpacity onPress={this.toUserInfo.bind(this)}>
                                 <Image source={require('./image/icon_drawer_head.jpg')} style={styles.icon}/>
                              </TouchableOpacity>
                                 <Text style={styles.textUserName}>汤姆克鲁斯</Text>
                              <TouchableOpacity onPress={()=>ToastAndroid.show('二维码',ToastAndroid.SHORT)}>
                                  <Image source={require('./image/icon_two_code.png')} style={styles.icon2}/>
                              </TouchableOpacity>
              			  </View>

                       <View style={{flex:1,flexDirection:'column'}}>

                          <Text style={styles.textEmail}>xxxxxxxxx@xxx.com</Text>
                       </View>
     			        </Image>

      	     			   <View style={{flex:1}}>
      	     			       <ListView
                            dataSource={this.state.dataSource}
                            renderRow={this.renderRow.bind(this)}
                         />
      	     			   </View>
                     <TouchableOpacity onPress={()=>{

                       var dialog = new DialogAndroid();
                       dialog.set(options);
                       dialog.show();

                     }
                    }>
                       <View >
                           <Text style={styles.loginout}>登出</Text>
                       </View>
                     </TouchableOpacity>


                 </View>



           	  	)
           }

   toDetail(date){

           this.state.drawer();
           // alert(date.title);
           if(date.title == '我的好友'){
              this.state.navigator.push({name:'friend',component: FriendList})
           }

           if(date.title == '控制面板'){
              // this.state.navigator.push({name:'timeLine',component: TimeLine})
            }

           if(date.title == '搜索'){
              this.state.navigator.push({name:'serachScreen',component: SearchScreen})
           }

           if(date.title == "标签"){
             this.state.navigator.push({name:'tag',component: Tag})

            }
   }
   toUserInfo(){
      this.state.navigator.push({name:'userInfo',component: UserInfo})
   }

  renderRow(date){


    return(
      <TouchableOpacity onPress={this.toDetail.bind(this,date)}>
        <View style={{flexDirection:'row'}}>
           <Image source={date.src} style={styles.iconItem} />
          <Text style={styles.textItem}> {date.title}</Text>
        </View>
      </TouchableOpacity>
    );
  }












}


var styles =  StyleSheet.create({
  main:{
		backgroundColor: '#fff',
		flex :1
  },

  head:{

       height:150,
       width:width

  },
  icon:{
       height:60,
       width:60,
       borderRadius:30,
       marginTop:10,
       marginLeft:20

  },
  icon2:{
       height:20,
       width:20,
       marginLeft:60,
      marginTop:40

  },
   iconItem:{
       height:20,
       width:20,
       marginLeft:20,
      marginTop:25

  },
  textItem:{
       height:35,
       width:80,
       fontSize:15,
       marginLeft:15,
       marginTop:25

  },
  textEmail:{
  	fontSize:20,

  	marginLeft:50,
  	marginTop:10,
  	color:'#fff'
  },
  textUserName:{
  	    marginTop:40,
  	    marginLeft:15,
  	    color:'#fff'

  },
  loginout:{fontSize:17,
    marginBottom:5,
    marginRight:10.,
    alignSelf:'flex-end'
  }
});
export default Drawer
