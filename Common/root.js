import React, {
  AppRegistry,
  Component,
  Navigator,
  Text,
  View
} from 'react-native';
import firstpage from '../Device/firstpage';
import Storage from 'react-native-storage';
import LoginActions from '../actions/loginAction'

import getStorage from '../Common/storage';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux'
var storage = getStorage().getInstance();

import Login from './Login';

global.Ticket = null;
class Infomanager extends React.Component {


  constructor(props){
    super(props);
    this.state={
      isFirstIN:null
    }
  }



  componentWillMount(){
     storage.load({
        key:'ticket'
     }).then(ret=>{

       global.Ticket = ret.Ticket;
      this.props.LoginActions.updateTicket(ret.Ticket);
        this.setState({
          isFirstIN:false
        });
     }).catch(err=>{
          this.setState({
            isFirstIN:true
          });
        }
     )



  }


  render()   {



  //  alert(this.state.isFirstIN);

    var defaultName = 'FirstPageComponent';
    var defaultComponent ;
    if(this.state.isFirstIN === null){
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center',flex:1}}>
            <Text >
              正在启动应用
           </Text>
     </View>
      );

    }else if(this.state.isFirstIN ===false ){
        defaultComponent = firstpage;
        return (
         <Navigator
              initialRoute={{ name: defaultName, component: defaultComponent }}
              configureScene={(route) => {
                return Navigator.SceneConfigs.FadeAndroid;
              }}
              renderScene={this.navigatorRenderScene} />
        );
    }else{
       defaultComponent =Login;
       return (
        <Navigator
             initialRoute={{ name: defaultName, component: defaultComponent }}
             configureScene={(route) => {
               return Navigator.SceneConfigs.FadeAndroid;
             }}
             renderScene={this.navigatorRenderScene} />
       );
    }


  }
         navigatorRenderScene(route, navigator){
          // if (route.index > 0) {
          //   navigator.pop();
          // }

            let Component = route.component;
            return (<Component {...route.params} navigator={navigator} />)
         }


}

function mapStateToProps(){
    return;
}

function mapDispatchToProps(dispatch){
    return {
      LoginActions :bindActionCreators(LoginActions,dispatch)
    }
}

export default connect (mapStateToProps,mapDispatchToProps)(Infomanager);
