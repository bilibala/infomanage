"use strict";

import React, {
    View,
    Component,
    StyleSheet,
    Image,
    Text,
    ListView,
} from 'react-native'

import TimeCell from './TimeCell'


class TimeCellContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {

    }


    render() {
        var {fileList} = this.props;
        var timeCells = [];
        var sum = Math.floor((fileList.length-1)/4);
        var dataIndex = 0;
        for (var i = 0; i <= sum; i++) {
            var count = (i==sum)? ((fileList.length % 4 == 0) ? 4 : (fileList.length%4)) : 4;
            var data = [];
            for(var j = 0;j < count;j++){
                data.push(fileList[dataIndex]);
                dataIndex++;
            }
            timeCells.push(
                <View key={i} style={styles.rowContainer}>
                    <TimeCell
                        count={count}
                        data={data}>
                    </TimeCell>
                </View>
                );
        }
        return (
            <View style={styles.container}>
                <View style={styles.textContainer}>
                    <View style={styles.timeContainer}>
                    <Image style={styles.time} source={require('./image/icon_time.png')}></Image>
                    </View>
                    <Text style={{fontSize:15}}>{this.props.time}</Text>
                </View>
                {timeCells}
            </View>
        );
    }
}

var styles = StyleSheet.create({
    container:{
        flex:1
    },
    rowContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    textContainer: {
        height:25,
        alignItems: 'center',
        flexDirection: 'row',
    },
    time:{
       height:18,
        width:18,
    },
    timeContainer:{
        height:18,
        width:18,
        marginLeft:5,
        marginRight:5,
    }

})

export default TimeCellContainer;