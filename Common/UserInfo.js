import React, {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ToolbarAndroid,
  ToastAndroid,
  Image
}
from 'react-native';


class UserInfo extends React.Component {

    constructor(props) {
      super(props);
      this.state = {

      };
    }

    componentDidMount() {


    }


    render() {

      return this._renderLightSence();

    }

    _renderLightSence() {
        return (
            <View style={styles.container}>
                       <ToolbarAndroid
                       navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha')}
                       title = {'个人信息'}
                       style={styles.toolbar}
                       onIconClicked={this.props.navigator.pop}
                       >
                       </ToolbarAndroid>

                       <View style={styles.container}>
                          <View style={styles.greyRow}>
                          </View>
                           <TouchableOpacity onPress={()=>ToastAndroid.show('头像',ToastAndroid.SHORT)}>
                          <View style={styles.rowContainer}>
                            <View style={styles.titleContainer}>
                              <Text style={styles.title}>
                              头像
                              </Text>
                              <Image source={require('../Device/image/icon_drawer_head.jpg')} style={styles.icon}/>
                            </View>
                          </View>
                          </TouchableOpacity>
                          <View style={styles.greyLine}>
                          </View>
                           <TouchableOpacity onPress={()=>ToastAndroid.show('用户名',ToastAndroid.SHORT)}>
                          <View style={styles.rowContainer}>
                            <View style={styles.titleContainer}>
                              <Text style={styles.title}>
                              用户名
                              </Text>
                              <Text style={styles.rightText}>
                              PANDA
                              </Text>
                            </View>
                          </View>
                          </TouchableOpacity>
                          <View style={styles.greyLine}>
                          </View>
                           <TouchableOpacity onPress={()=>ToastAndroid.show('账号',ToastAndroid.SHORT)}>
                          <View style={styles.rowContainer}>
                            <View style={styles.titleContainer}>
                              <Text style={styles.title}>
                              账号
                              </Text>
                            </View>
                            <Text style={styles.rightText}>
                              AAAAAAAAAA
                              </Text>
                          </View>
                          </TouchableOpacity>
                          <View style={styles.greyLine}>
                          </View>
                           <TouchableOpacity onPress={()=>ToastAndroid.show('二维码',ToastAndroid.SHORT)}>
                          <View style={styles.rowContainer}>
                            <View style={styles.titleContainer}>
                              <Text style={styles.title}>
                              我的二维码
                              </Text>
                               <Image source={require('./image/icon_two_code.png')} style={styles.iconTwoCode}/>
                            </View>
                          </View>
                          </TouchableOpacity>
                          <View  style={styles.greyRow}>
                          </View>
                           <TouchableOpacity onPress={()=>ToastAndroid.show('地区',ToastAndroid.SHORT)}>
                          <View style={styles.rowContainer}>
                            <View style={styles.titleContainer}>
                              <Text style={styles.title}>
                              地区
                              </Text>
                            </View>
                            <Text style={styles.rightText}>
                              北京
                              </Text>
                          </View>
                          </TouchableOpacity>
                          <View style={styles.greyLine}>
                          </View>
                           <TouchableOpacity onPress={()=>ToastAndroid.show('性别',ToastAndroid.SHORT)}>
                          <View style={styles.rowContainer}>
                            <View style={styles.titleContainer}>
                              <Text style={styles.title}>
                              性别
                              </Text>
                              
                            </View>
                          </View>
                          </TouchableOpacity>
                          <View style={styles.greyLine}>
                          </View>
                           <TouchableOpacity onPress={()=>ToastAndroid.show('个性签名',ToastAndroid.SHORT)}>
                          <View style={styles.rowContainer}>
                            <View style={styles.titleContainer}>
                              <Text style={styles.title}>
                              个性签名
                              </Text>
                            </View>
                            <Text style={styles.rightText}>
                              嚄嚄嚄嚄嚄嚄嚄嚄嚄嚄嚄嚄嚄
                              </Text>
                          </View>
                          </TouchableOpacity>
                       </View>



      < /View>
    )

  }




}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
  },
  toolbar: {
    backgroundColor: '#6d9eeb',
    height: 56,
  },
  greyRow:{
    height:25,
    backgroundColor: '#f2f2f2',
  },
  greyLine:{
    height:1,
    backgroundColor: '#f2f2f2',
    marginLeft:15,
  },
  rowContainer:{
    height:55,
    flexDirection: 'row',
  },
  titleContainer:{
    justifyContent: 'center',
    flex:1
  },
  title:{
    fontSize:16,
    marginLeft:15,
    color:'#273237'
  },
  icon:{
    width:36,
    height:36,
    position: 'absolute',
    top: 12,
    right: 20,
    borderRadius:18,
  },
  iconTwoCode:{
    width:30,
    height:30,
    position: 'absolute',
    top: 12,
    right: 16,
  },
  rightText:{
    fontSize:16,
    color:'#989898',
    position: 'absolute',
    top: 15,
    right: 20,
  }
  // text: {
  //   fontSize: 30,
  // },
  // image: {
  //   width: 90,
  //   height: 90,
  // },
  // imageWapper: {
  //   flexDirection: 'column',
  //   alignItems: 'center', 
  //   justifyContent: 'center',
  //   marginTop:20
  // },
  // textWapper: {
  //   flexDirection: 'column',
  //   alignItems: 'center',
  //    justifyContent: 'center',
  //    marginTop:30
  // }
});


export default UserInfo