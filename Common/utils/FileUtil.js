var FILEICON = {
  mov : require('../image/fileicon/mov.png'),
  mp3 : require('../image/fileicon/mp3.png'),
  mpeg : require('../image/fileicon/mpeg.png'),
  other : require('../image/fileicon/other.png'),
  pdf : require('../image/fileicon/pdf.png'),
  png : require('../image/fileicon/png.png'),
  ppt : require('../image/fileicon/pptx_win.png'),
  proj : require('../image/fileicon/proj.png'),
  psd : require('../image/fileicon/psd.png'),
  pst : require('../image/fileicon/pst.png'),
  pub : require('../image/fileicon/pub.png'),
  rar : require('../image/fileicon/rar.png'),
  readme : require('../image/fileicon/readme.png'),
  settings : require('../image/fileicon/settings.png'),
  text : require('../image/fileicon/text.png'),
  txt : require('../image/fileicon/text.png'),
  tiff : require('../image/fileicon/tiff.png'),
  url : require('../image/fileicon/url.png'),
  vsd : require('../image/fileicon/vsd.png'),
  wav : require('../image/fileicon/wav.png'),
  wma : require('../image/fileicon/wma.png'),
  wmv : require('../image/fileicon/wmv.png'),
  xlsx : require('../image/fileicon/xlsx_win.png'),
  xls : require('../image/fileicon/xlsx_win.png'),
  zip : require('../image/fileicon/zip.png'),
  accdb : require('../image/fileicon/accdb.png'),
  avi : require('../image/fileicon/avi.png'),
  bmp : require('../image/fileicon/bmp.png'),
  css : require('../image/fileicon/css.png'),
  doc : require('../image/fileicon/docx_win.png'),
  docx : require('../image/fileicon/docx_win.png'),
  eml : require('../image/fileicon/eml.png'),
  eps : require('../image/fileicon/eps.png'),
  fla : require('../image/fileicon/fla.png'),
  gif : require('../image/fileicon/gif.png'),
  html : require('../image/fileicon/html.png'),
  ini : require('../image/fileicon/ini.png'),
  jpeg : require('../image/fileicon/jpeg.png'),
  midi : require('../image/fileicon/midi.png'),
  mp4 : require('../image/fileicon/avi.png'),
}
function FileIcon(webdavpath,ticket){
  let index = webdavpath.lastIndexOf('.');
  let type = webdavpath.substring(index+1);
  let image;
  if(type){
    if(type == "png" || type == "jpg" || type == "jpeg" || type == "gif"){
      var url = "http://42.159.143.17:6080/alfresco/webdav"+webdavpath+"?ticket="+ticket;
      image = {uri : url};
    }else{
      image = FILEICON[type];
      if(!image){
        image = FILEICON['other'];
      }
    }
  }else{
    image = FILEICON['other'];
  }
  return image;
}
function FileIconForSelect(fileid,fileName,ticket){
  let index = fileName.lastIndexOf('.');
  let type = fileName.substring(index+1);
  let image;
  if(type){
    if(type == "png" || type == "jpg" || type == "jpeg" || type == "gif"){
      var url = "http://http://42.159.143.17:6080/alfresco/s/api/node/content/workspace/SpacesStore/"+fileid+"?ticket="+ticket;
      image = {uri : url};
    }else{
      image = FILEICON[type];
      if(!image){
        image = FILEICON['other'];
      }
    }
  }else{
    image = FILEICON['other'];
  }
  return image;
}



module.exports = {FileIcon};
