
function formatDate(now) {
	var now=new Date(parseInt(now)); 
	var year=now.getYear(); 
	var month=now.getMonth()+1; 
	var date=now.getDate(); 
	var hour=now.getHours(); 
	var minute=now.getMinutes(); 
	var second=now.getSeconds(); 
	return year+"-"+month+"-"+date; 
}
module.exports = {formatDate}
