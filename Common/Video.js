'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  WebView,
} from 'react-native';


var Web = React.createClass({
  render: function() {
    return (
      <View style={{flex:1}}>
        <Text style={{height:40}}>简单的网页显示</Text>
        <WebView style={styles.webview_style} 
          source={{uri:this.props.uri,method:'GET',headers:{'Content-Type':'application/json','Authorization': 'Basic dHcuZmVuZ0BoYWloYW5neXVuLmNvbToxMTExMTE='}}}          startInLoadingState={true}
          domStorageEnabled={true}
          javaScriptEnabled={true}
          renderError={()=> console.log("renderError")}
          renderLoading={()=> console.log("renderLoading")}
          onLoad={()=> console.log("onLoad")}
          onLoadEnd={()=> console.log("onLoadEnd")}
          onLoadStart={()=> console.log("onLoadEnd")}
          >
        </WebView>
      </View>
    );
  },
});
var styles = StyleSheet.create({
    webview_style:{  
       backgroundColor:'#00ff00',   
    }
});
export default Web