'use strict'

import * as types from '../actions/actionTypes';

const initialState = {
  isLoading:false,
  token:null,
  isSuccess:false,
  ticket:null
};

export default function Login(state=initialState,action = {}  ){
	switch(action.type){
		case types.Login:

		   return{
		   	...state,
		   	isLoading:true,
		   };
    case types.LoginFail:

   		   return{
   		   	...state,
   		   	isLoading:false,
   		   };
     case types.LoginSuccess:
         return{
          ...state,
          isLoading:false,
          token:action.token,
          isSuccess:true
         };
      case types.UpdateTicket:
        return {
          ...state,
          ticket:action.ticket
        };

		default:
		   return state;
	}

}
