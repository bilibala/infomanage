import * as types from '../actions/actionTypes';

const initialState = {
  // navigator:null
};

function navigator(state = initialState,action){
	if(action.type == types.InitNavigator){
		return {
			...state,
			navigator: action.navigator
		}
	}
}

module.exports = {navigator}