'use strict'

import * as types from '../actions/actionTypes';

const initialState = {
  isLoading:false,
  date:[],
  isShowResult:false
};

export default function Search(state=initialState,action = {}  ){
	switch(action.type){
		case types.Search:

		   return{
		   	...state,
		   	isLoading:true,
		   };

     case types.SearchCompelete:
         return{
          ...state,
          isLoading:false,
          date:action.date
         };

		default:
		   return state;
	}

}
