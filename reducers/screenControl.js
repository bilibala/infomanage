'use strict'

import * as types from '../actions/actionTypes';

const initialState = {
      isopen :null
};

export default function screenControl (state = initialState ,action = {}){

       switch (action.type) {
         case types.OpenWeb:
             return {
               ...state,
               isopen:action.isopen
             }
          case types.CloseWeb:
              return {
               ...state,
              isopen:action.isopen
             }


         default:
            return state;

       }

}
