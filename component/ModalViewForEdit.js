'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Navigator,
  BackAndroid,
  TouchableOpacity
} from 'react-native';

class SecondView extends Component{
 
  render(){
      return (
         <View>
          <Modal style={styles.opModal} visible={this.state.opVisible}>
            <View style={{flexDirection:'row'}}>
              <View style={{flex:1}}>
                  <Image
                    style={{height:50}}
                    source={require('./img/videooff.png')} />
              <View style={{flex:1}}>
                  <Image
                    style={{height:50}}
                    source={require('./img/videooff.png')} />
              </View>
              <View style={{flex:1}}>
                  <Image
                    style={{height:50}}
                    source={require('./img/videooff.png')} />
              </View>
              <View style={{flex:1}}>
                  <Image
                    style={{height:50}}
                    source={require('./img/videooff.png')} />
              </View>
              <View style={{flex:1}}>
                  <Image
                    style={{height:50}}
                    source={require('./img/videooff.png')} />
              </View>
              
            </View>
          </Modal>
         </View>
      );
  }
}

const styles = StyleSheet.create({
   opModal: {
    bottom: 0,
    backgroundColor: 'red',
    overflow: 'visible'
  },
});

module.exports = SecondView;