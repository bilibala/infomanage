'use strict';

var React = require('React');
var Image = require('Image');
var StyleSheet = require('StyleSheet');
var { Text } = require('react-native');
var TouchableHighlight = require('TouchableHighlight');
var View = require('View');
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import RNFileSearch from './RNFileSearch';
import getStorage from '../Common/storage.js';
import {FileIcon} from '../Common/utils/FileUtil'



class FileCell extends React.Component {
  props: {
    file: any;
    onPress: ?() => void;
  };

  render() {
    const {file} = this.props;
    const icon = FileIcon(file.webdavpath,this.props.ticket);

    const cell = (
          <View style={styles.container}>
            <Image
              source={icon}
              style={styles.thumbnail}
              ></Image>
            <View>
              <Text style={styles.title} numberOfLines={1}>{file.name}</Text>
              <Text style={styles.year}>{getLocalTime(file.updated)}</Text>
            </View>
          </View>
    );

    return (
      <TouchableHighlight onPress={()=>this._pressRow(file)}>
        {cell}
        </TouchableHighlight>
    );
  }

  _pressRow(file){

      getStorage().getTicket().then( ret => {
        var ticket = ret.Ticket;
        var url = "http://42.159.143.17:6080/alfresco/webdav"+file.webdavpath+"?ticket="+ticket;
        RNFileSearch.openOnlineFile(encodeURI(url),file.name);
      }).catch( err => {

      });
  }
}

var getLocalTime = function(time){
 var now = new Date(parseInt(time))
 var year=now.getFullYear();
 var month=now.getMonth()+1;
 var date=now.getDate();
 var hour=now.getHours();
 var minute=now.getMinutes();
 var second=now.getSeconds();
 return   year+"-"+month+"-"+date+"   "+hour+":"+minute+":"+second;
}


var styles = StyleSheet.create({

  container: {
    height: 65,
    flexDirection: 'row',
    backgroundColor:'#f5f5f5',
  },
  thumbnail: {
    marginTop:10,
    marginLeft:15,
    width: 40,
    height: 50,
    // resizeMode: 'stretch'
  },
  title:{
    marginBottom: 8,
    marginTop:13,
    marginLeft:15,
    width:250,
    color:'black',
    fontSize:16
  },
  year:{
    fontSize: 12,
    marginTop:-5,
    marginLeft:15,
  },
});


function mapStateToProps(store){
    return {
      ticket : store.Login.ticket
    };
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({},dispatch);
}

export default connect (mapStateToProps,mapDispatchToProps)(FileCell);

module.exports = FileCell;
