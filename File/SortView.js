'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  BackAndroid,
  ListView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions
} from 'react-native';
var ListComponent = require('./ListComponent');
var RNFileSearch = require('./RNFileSearch');

class SortView extends Component{

  constructor(props) {
    super(props);
    this.state = (
      {
        // type: this.props.type,
        cloudDatas: this.props.cloud,
        navigator:this.props.navigator,
        downloadCenter:this.props.downloadCenter

      }
    );
  }
  
  render(){
    return (
      <ListComponent type={this.props.type} navigator={this.props.navigator} data={this.state.cloudDatas} downloadCenter={this.props.downloadCenter}/>

    );
  }
}

module.exports = SortView;
