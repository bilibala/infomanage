'use strict';
var { NativeModules } = require('react-native');

var RCTMediaControl= NativeModules.MediaControl;

var MediaControl = {

   play: function(
     filePath: string
   ): void{
     RCTMediaControl.play(filePath);
   },

};
module.exports = MediaControl;
