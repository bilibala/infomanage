import React, {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ToolbarAndroid,
  Image
}
from 'react-native';
import TagListView from './TagListView'
import TagDetail from './TagDetail'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import TagAction from '../actions/TagAction'

const Tags = [{
  name:"标签1"
},{
  name:"标签2"
},{
  name:"标签3"
},{
  name:"标签4"
},{
  name:"标签5"
},{
  name:"标签6"
},{
  name:"标签7"
},{
  name:"标签8"
},{
  name:"标签9"
},{
  name:"标签10"
},{
  name:"标签11"
}]


class Tag extends React.Component {

    constructor(props) {
      super(props);
      this.state = {

      };
    }
    componentDidMount() {
      const {TagAction} = this.props;
      TagAction.fetchTag();
    }


    render() {
      return this._renderLightSence();
    }

    _renderLightSence() {
        return (
            <View style={styles.container}>
                       <ToolbarAndroid
                       navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha')}
                       title = {'标签'}
                       style={styles.toolbar}
                       onIconClicked={this.props.navigator.pop}
                       >
                       </ToolbarAndroid>

                       <View style={styles.container}>
                          <TagListView
                            tags = {this.props.tags}
                            navigator = {this.props.navigator}
                            tagAction = {this.props.TagAction}>
                          </TagListView>
                        </View>
      </View>
    )

  }



}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'

  },
  toolbar: {
    backgroundColor: '#6d9eeb',
    height: 56,
  }
})





function mapStateToProps(store){
    return {tags: store.Tag.tags};
}

function mapDispatchToProps(dispatch){
    return {
      TagAction:bindActionCreators(TagAction,dispatch)
    }
}

export default connect (mapStateToProps,mapDispatchToProps)(Tag);
