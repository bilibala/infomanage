'use strict';
import React, {
    Image,
    View,
    Text,
    Component,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    ToastAndroid,
} from 'react-native';


var PureListView = require('../Common/PureListView');
var TagCell = require('./TagCell');
import TagDetail from './TagDetail'


type Tag = any;

type Props = {
  tags: Array<Tag>;
  navigator: any;
};

class TagListView extends React.Component {
  props: Props;

  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <PureListView
        data={this.props.tags}
        renderRow={this.renderRow.bind(this)}
        renderEmptyList={this.renderEmptyList.bind(this)}
        {...this.props}
      />
    );
  }


  renderRow(tag: Tag,sID,rID){
    var cell;
    if(rID == 0){
      cell = (
      <TouchableHighlight  onPress={this.addTag.bind(this)}>
      <View style={styles.cells}>
        <View style={styles.iconContainer}>
          <Image source={require('./img/icon_add.png')} style={styles.icon}>
          </Image>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.name}>
            {"添加新标签"}
          </Text>
        </View>
      </View>
      </TouchableHighlight>
    );
    }else{
      cell = (<TagCell
        tag={tag}
        onPress={() => this.openTagDetails(tag)}
      />);
    }
    return (
      <View style = {{flex:1}}>
        {cell}
      </View>
    );
  }
  addTag(){
    this.props.tagAction.addTag("哈哈啊");
  }


  renderEmptyList(): ?ReactElement {
    return (
      <View>
      </View>
    );
  }
  openTagDetails(tag){
    this.props.navigator.push({name:"TagDetail",component:TagDetail,params:{tag:tag}})
  }
}

var styles = StyleSheet.create({
  cells:{
    height: 40,
    paddingHorizontal: 10,
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  iconContainer: {
    height: 40,
    paddingHorizontal: 10,
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  icon:{
    height:28,
    width:28,
  },
  textContainer: {
    height: 40,
    flex:1,
    paddingHorizontal: 10,
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  name: {
    flex: 1,
    fontSize: 16,
    marginTop:10,
    marginBottom:0
  },
  pointContainer:{
    height:40,
    width:40,
    marginRight:20,
    alignItems: 'center',

  },
  point:{
    height:28,
    width:28,
  }
});


module.exports = TagListView;
