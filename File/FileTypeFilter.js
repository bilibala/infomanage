 
  export function getFileByType(list,type){
      var pic = ['bmp','gif','jpeg','png','jpg','psd','hdri']
  var doc = ['txt','doc','xls','wpd','pdf']
  var ppt = ['ppt']
  var music = ['mp3','midi','wave','cd','wma']
  var video = ['mp4','mpeg','avi','mov','rmvb','mkv','flv']
      var contains  = function(arr, obj) {
          var i = arr.length;
          while (i--) {
              if (arr[i] == obj) {
                  return true;  
              }
          } 
          return false;
      }
      switch(type){
    		case 'pic':{
    			var picList = [];
    			list.forEach(function(file){
    				var i = file.name.indexOf('.');
    				if(i){
    					var end = file.name.substring(i+1);
    					if(contains(pic,end)){
    						picList.push(file);
    					}
    				}
    			});
    			return picList;
    		}
    		case 'doc':{
    			var docList = [];
    			list.forEach(function(file){
    				var i = file.name.indexOf('.');
    				if(i){
    					var end = file.name.substring(i+1);
    					if(contains(doc,end)){
    						docList.push(file);
    					}
    				}
    			});
    			return docList;
    		}
    		case 'ppt':{
    			var pptList = [];
    			list.forEach(function(file){
    				var i = file.name.indexOf('.');
    				if(i){
    					var end = file.name.substring(i+1);
    					if(contains(ppt,end)){
    						pptList.push(file);
    					}
    				}
    			});
    			return pptList;
    		}
    		case 'music':{
    			var musicList = [];
    			list.forEach(function(file){
    				var i = file.name.indexOf('.');
    				if(i){
    					var end = file.name.substring(i+1);
    					if(contains(music,end)){
    						musicList.push(file);
    					}
    				}
    			});
    			return musicList;
    		}
    		case 'video':{
    			var videoList = [];
    			list.forEach(function(file){
    				var i = file.name.indexOf('.');
    				if(i){
    					var end = file.name.substring(i+1);
    					if(contains(video,end)){
    						videoList.push(file);
    					}
    				}
    			});
    			return videoList;
    		}
    		default :{
    			return [];
    		}
  	  }
  }
    export function getMimeType (filename){

      var pic = ['bmp','gif','jpeg','png','psd','png','hdri']
  var doc = ['txt','doc','xls','wpd','pdf']
  var ppt = ['ppt']
  var music = ['mp3','midi','wave','cd','wma']
  var video = ['mp4','mpeg','avi','mov','rmvb','mkv','flv']    

  var contains  = function(arr, obj) {
          var i = arr.length;
          while (i--) {
              if (arr[i] == obj) {
                  return true;  
              }
          } 
          return false;
      }

      var i = filename.indexOf('.');
      if(i){
        var end = filename.substring(i+1);
        if(contains(video,end)){
          return "video/*"
        }
        if(contains(pic,end)){
          return "image/*"
        }
        if(contains(doc,end)){
          return "text/html"
        }
        if(contains(music,end)){
          return "audio/mpeg"
        }
        if(contains(ppt,end)){
          return "text/html"
        }

      }
      return "text/html";
    }
