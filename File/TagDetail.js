'use strict';
import React, {

  Component,
  StyleSheet,
  ToastAndroid,
  Text,
  View,
  Image,
  ListView,
  TouchableOpacity,
  ToolbarAndroid,
  TextInput,
  Dimensions
} from 'react-native';
import PureListView from '../Common/PureListView'
import FileCell from './FileCell'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import TagAction from '../actions/TagAction'
 var toolbarActions =[


  {title: '添加内容', show: 'never'},
  {title:'删除标签',show:'never'},
  {title:'文件管理',show:'never'}

];
let _navigator = null;



 class TagDetail extends Component{


    constructor(props) {
      super(props);
      _navigator = this.props.navigator;
       this.state ={
       	 tag:this.props.tag,
         tagName:this.props.tag,
         files:[]
       }

   }

 componentDidMount() {
   const {tag} = this.props;
   this.props.TagAction.fetchFileByTag(tag);
 }


	    render(){
	    	return(
          <View style={styles.main}>
	    		<ToolbarAndroid
                       title = {this.state.tagName}
                       actions={toolbarActions}
                       style={styles.toolbar}
                       navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha')}
                        onActionSelected={this.onActionSelected.bind(this)}
                        onIconClicked={this.props.navigator.pop}

                       >
                       </ToolbarAndroid>

                <View style={styles.container}>
                    <Text style={styles.title}>
                      {'标签名称'}
                    </Text>
                    <TextInput
                      style={{marginLeft:18,height:50,width:230,fontSize:18}}
                      value={this.state.tagName}
                      onChangeText={this.oneTagText.bind(this)}
                      onSubmitEditing ={this.changeTagName.bind(this)}
                    />
                    <Text style={styles.title}>
                      {'标签内容'}
                    </Text>
                    <PureListView
                      data={this.props.files}
                      renderRow={this.renderRow.bind(this)}
                      renderEmptyList={this.renderEmptyList.bind(this)}
                      {...this.props}
                    />
                  </View>
              </View>
                    )
	    }

      changeTagName(){

      }

      renderRow(file){
        return (
          <FileCell
            file={file}
            onPress={() => this.openFile(file)}
            >
          </FileCell>
        );
      }

      renderEmptyList(){
        return (
          <View>
          </View>
        );
      }

      openFile(file){

      }

	    onActionSelected(position){
        var {TagAction,tag} = this.props;
        if(position == 1){
          TagAction.removeTag(tag,this.callback);
        }
	    }
      callback(){
        _navigator.pop();
      }

	    oneTagText(text){

		    this.setState({
		        tagName:text ,
		    });

    }
}

const styles = StyleSheet.create({
  toolbar: {
    backgroundColor: '#6d9eeb',
    height: 56,
  },
  container:{
    flexDirection:'column',
    flex:1,
    backgroundColor:'#f5f5f5'
  },
  title:{
    color:'#4b4b4d',
    fontSize:18,
    marginTop:5,
    marginBottom:5,
    marginLeft:10
  }


  });

   function mapStateToProps(store){
       return {files : store.Tag.files};
   }

   function mapDispatchToProps(dispatch){
       return {
          TagAction:bindActionCreators(TagAction,dispatch)
        }
   }

   export default connect (mapStateToProps,mapDispatchToProps)(TagDetail);
