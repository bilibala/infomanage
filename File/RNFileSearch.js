'use strict';
var { NativeModules } = require('react-native');

var RCTRNFileSearch= NativeModules.RNFileSearch;

var RNFileSearch = {

  getImageResult: function(

  ): void {
    RCTRNFileSearch.getImageResult();
  },

   getVideoResult: function(

  ): void {
    RCTRNFileSearch.getVideoResult();
  },

   getAudioResult: function(

  ): void {
    RCTRNFileSearch.getAudioResult();
  },

   getPptResult: function(

  ): void {
    RCTRNFileSearch.getPptResult();
  },

   getWpsResult: function(

  ): void {
    RCTRNFileSearch.getWpsResult();
  },

   play: function(

   ): void{
     RCTRNFileSearch.play();
   },
   openFile:function(
    url:string,
    base:string,
    type:string
    ):void{
    RCTRNFileSearch.openFile(url,base,type);
  },
  openLocalFile:function(
    path:string
  ):void{
    RCTRNFileSearch.openLocalFile(path);
  },
  openOnlineFile:function(
    path:string,
    filename:string
  ):void{
    RCTRNFileSearch.openOnlineFile(path,filename);
  }

};

module.exports = RNFileSearch;
