'use strict';

var React = require('React');
var Image = require('Image');
var StyleSheet = require('StyleSheet');
var { Text } = require('react-native');
var TouchableHighlight = require('TouchableHighlight');
var View = require('View');


class TagCell extends React.Component {
  props: {
    tag: any;
    onPress: ?() => void;
  };

  render() {
    const {tag} = this.props;


    const cell = (
      <View style={styles.cells}>
        <View style={styles.iconContainer}>
          <Image source={require('./img/icon_tag.png')} style={styles.icon}>
          </Image>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.name}>
            {tag}
          </Text>
        </View>
        <View style={styles.pointContainer}>
          <Image source={require('./img/icon_point.png')} style={styles.point}>
          </Image>
        </View>
      </View>
    );

    return (
      <TouchableHighlight underlayColor="#3C5EAE" onPress={this.props.onPress}>
        {cell}
      </TouchableHighlight>
    );
  }
}


var styles = StyleSheet.create({
  cells:{
    height: 40,
    paddingHorizontal: 10,
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  iconContainer: {
    height: 40,
    paddingHorizontal: 10,
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  icon:{
    height:28,
    width:28,
  },
  textContainer: {
    height: 40,
    flex:1,
    paddingHorizontal: 10,
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  name: {
    flex: 1,
    fontSize: 16,
    marginTop:10,
    marginBottom:0
  },
  pointContainer:{
    height:40,
    width:40,
    marginRight:20,
    alignItems: 'center',
    justifyContent:'center'

  },
  point:{
    height:18,
    width:18,
  }
});

module.exports = TagCell;
