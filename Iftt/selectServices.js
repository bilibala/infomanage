'use strict';

import React, {

  Component,
  StyleSheet,
  ToolbarAndroid,
  Text,
  View,
  Image,
  ListView,
  TouchableOpacity,
  Dimensions
}
from 'react-native';

import Loader from '../component/loader/Loader';

var SERVICES = [{
  "id": "11494",
  "title": "邮箱",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11496",
  "title": "天气",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}, {
  "id": "11495",
  "title": "信息",
  "icon": "",
}];
var SERVICES_LIST = []
var TYPE = {
  local: "本地服务",
  remote: "云服务",
}

class SelectServices extends Component {

  constructor(props) {
    super(props);
    this.state = {
      type: ''
    }
    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.state = {
      dataSource: null,
      isload: false,
      ds: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      }),
    }
  }

  componentDidMount() {
    this.setState({
      isload: true,
    });
    if (this.props.type == 'trigger') {
      this.setState({
        type: '选择触发条件'
      })
    } else {
      this.setState({
        type: '选择执行动作'
      })
    }
    var j = -1
    for (var i = 0; i < SERVICES.length; i++) {
      if (i % 3 == 0) {
        j++
        SERVICES_LIST[j] = []
      }

      SERVICES_LIST[j].push(SERVICES[i])
    }

    this.setState({
      dataSource: this.state.ds.cloneWithRows(SERVICES_LIST),
      isload: true
    });
  }



  render() {

    if (!this.state.isload) {
      return (
        <View style={{alignItems:'center'}}>
	            	<Loader
	             size={37}
	             isVisible ={true}
                 type={'ThreeBounce'}
	             color={'#6d9eeb'}
	             />
	             </View>

      );

    } else {
      return (

        <View style={styles.container}>
	           <ToolbarAndroid
                       navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha')}
                       title = {this.state.type}
                       style={styles.toolbar}
                       onIconClicked={this.props.navigator.pop}
                       >
                       </ToolbarAndroid>
                       <View style={styles.container}>
                           <ListView
                              dataSource={this.state.dataSource}
                              renderRow={this.renderRow.bind(this)}
                           />
                        </View>
        </View>
      );
    }
  }

  renderRow(serviceInfo, sectionID, rowID) {

    if (rowID == 0) {
      return this._titleRender(TYPE.local)
    }
    if (rowID == 3) {
      return this._titleRender(TYPE.remote)
    }

    // alert(serviceInfo)
    // if (serviceInfo[0] != undefined) {
    var imgOne = serviceInfo.length > 0 && serviceInfo[0] != undefined ? serviceInfo[0].title : ' ';
    var imgTwo = serviceInfo.length > 1 && serviceInfo[1] != undefined ? serviceInfo[1].title : ' ';
    var imgThree = serviceInfo.length > 2 && serviceInfo[2] != undefined ? serviceInfo[2].title : '  ';
    // } else {
    //   var imgOne = '无';
    //   var imgTwo = '无';
    //   var imgThree = '无';
    // }
    if (serviceInfo.length > 2 && serviceInfo[2] != undefined) {
      return (
        <View style={styles.rowContainer}>
      <View style={styles.serviceContainer}>
        <Image source={require('./image/IFTTT/message.png')} style={styles.serviceIcon}/>
        <Text style={styles.serviceText}>
       {imgOne}
       </Text>
       </View>
       <View style={styles.serviceContainer}>
        <Image source={require('./image/IFTTT/message.png')} style={styles.serviceIcon}/>
        <Text style={styles.serviceText}>
       {imgTwo}
       </Text>
       </View>
       <View style={styles.serviceContainer}>
        <Image source={require('./image/IFTTT/message.png')} style={styles.serviceIcon}/>
      <Text  style={styles.serviceText}>
       {imgThree}
       </Text> 
       </View>
       </View>);
    }
    if (serviceInfo.length > 1 && serviceInfo[1] != undefined) {
      return (
        <View style={styles.rowContainer}>
      <View style={styles.serviceContainer}>
        <Image source={require('./image/IFTTT/message.png')} style={styles.serviceIcon}/>
        <Text style={styles.serviceText}>
       {imgOne}
       </Text>
       </View>
       <View style={styles.serviceContainer}>
        <Image source={require('./image/IFTTT/message.png')} style={styles.serviceIcon}/>
        <Text style={styles.serviceText}>
       {imgTwo}
       </Text>
       </View>
        <View style={styles.serviceContainer}>
        
      <Text style={styles.serviceText}>
       {imgThree}
       </Text> 
       </View>
  </View>);
    }
    if (serviceInfo.length > 0 && serviceInfo[0] != undefined) {
      return (
        <View style={styles.rowContainer}>
      <View style={styles.serviceContainer}>
        <Image source={require('./image/IFTTT/message.png')} style={styles.serviceIcon}/>
        <Text style={styles.serviceText}>
       {imgOne}
       </Text>
       </View>
       </View>);
    }
  }

  _titleRender(title) {
    return (<View style={styles.titleContainer}>
              <Text style={styles.titleText}>
              {title}
              </Text>
            </View>);

  }
  _contentRender(services) {

  }
}


const styles = StyleSheet.create({

  container: {
    flex: 1,
  },
  toolbar: {
    backgroundColor: '#6d9eeb',
    height: 56,
  },
  titleContainer: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    height: 50,
  },
  titleText: {
    flex: 1,
    fontSize: 25,
    color: '#333333',
    textAlign: 'center',
    marginTop: 8
  },
  rowContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  serviceContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10
  },
  serviceIcon: {
    width: 60,
    height: 60,

  },
  serviceText: {
    textAlign: 'center',
  }

});


export default SelectServices