import React, {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ToolbarAndroid,
  Image
}
from 'react-native';

import TagListView from './TagListView.js'
const Tags = [{
  name:"标签1"
},{
  name:"标签2"
},{
  name:"标签3"
},{
  name:"标签4"
},{
  name:"标签5"
},{
  name:"标签6"
},{
  name:"标签7"
},{
  name:"标签8"
},{
  name:"标签9"
}{
  name:"标签10"
},{
  name:"标签11"
}]


class Tag extends React.Component {

    constructor(props) {
      super(props);
      this.state = {

      };
    }
    componentDidMount() {

    }


    render() {
      return this._renderLightSence();
    }

    _renderLightSence() {
        return (
            <View style={styles.container}>
                       <ToolbarAndroid
                       navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha')}
                       title = {'标签'}
                       style={styles.toolbar}
                       onIconClicked={this.props.navigator.pop}
                       >
                       </ToolbarAndroid>

                       <View style={styles.container}>
                          <TagListView 
                            tags = {Tags}
                            navigator = {this.props.navigator}>
                          </TagListView>
                        </View>
      </View>
    )

  }



}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'

  },
  toolbar: {
    backgroundColor: '#6d9eeb',
    height: 56,
  }
}


export default Tag