'use strict';

import React, {

  Component,
  StyleSheet,

  Text,
  View,
  Image,
  ListView,
  TouchableOpacity,
  Dimensions,
  Switch
}
from 'react-native';
import taskDetail from './taskDetail'
import Loader from '../component/loader/Loader';

var TASK_LIST = [{
  "id": "11111",
  "info": "卡手机卡黑客黑客技术大会扩大化卡的活动",
  "trigger": "",
  "action": "",
  "status": true,
}, {
  "id": "1111",
  "info": "卡手机卡黑客黑客技术大会扩大化卡的活动",
  "trigger": "",
  "action": "",
  "status": false,

}, {
  "id": "111",
  "info": "卡手机卡黑客黑客技术大会扩大化卡的活动",
  "trigger": "",
  "action": "",
  "status": false,
}];



class TaskList extends Component {

  constructor(props) {
    super(props);
    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.state = {
      dataSource: null,
      isload: false,
      ds: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      }),
    }

  }

  componentDidMount() {


    this.setState({
      dataSource: this.state.ds.cloneWithRows(TASK_LIST),
      isload: true
    });
  }



  render() {



    if (!this.state.isload) {
      return (
        <View style={{alignItems:'center'}}>
	            	<Loader
	             size={37}
	             isVisible ={true}
                 type={'ThreeBounce'}
	             color={'#6d9eeb'}
	             />
	             </View>

      );

    } else {
      return (

        <View style={styles.container}>
	             <ListView
	                dataSource={this.state.dataSource}
	                renderRow={this.renderRow.bind(this)}
	             />
             </View>
      );
    }
  }

  renderRow(taskInfo, sectionID, rowID) {

    var onValueChange = function(value) {
      TASK_LIST[rowID].status = value;
      this.setState({
        dataSource: this.state.ds.cloneWithRows(TASK_LIST),
      });
    };
    return (


      <TouchableOpacity onPress={()=>this._pressRow(taskInfo)}>
             <View style ={styles.item}>
               <View style ={styles.taskTop}>
                  <Image source={require('./image/IFTTT/message.png')} style={styles.trigger}/>
                  <View style={{flex: 1}}>
                  <Image source={require('./image/IFTTT/weather.png')} style={styles.action}/>
                  </View>
                  <View style={styles.switch}>
                  <Switch  
                                  disabled={false}
                                  onValueChange={onValueChange.bind(this)}
                                  value={TASK_LIST[rowID].status}
                                  ></Switch>
                                  </View>
                </View>
                <View  style ={styles.taskBottom}>
                  <Text style={styles.text}>
                      {taskInfo.info}
                  </Text>
                </View>
             </View>
        </TouchableOpacity>
    )


  }

  _pressRow(taskInfo) {
    this.props.navigator.push({
      name: 'taskDetail',
      component: taskDetail
    })
  }
}


const styles = StyleSheet.create({

  container: {
    flex: 1,
  },
  item: {
    flexDirection: 'column',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    height: 75,
  },
  taskTop: {
    flex: 1,
    flexDirection: 'row',
    height: 45,
  },
  taskBottom: {
    flexDirection: 'row',
    height: 30,
  },
  trigger: {
    width: 45,
    height: 45,
    marginRight: 10,
  },
  action: {
    width: 45,
    height: 45,
  },
  text: {

  },
  switch: {
    alignItems: 'flex-end',
    marginTop:8,
  },
});


module.exports = TaskList;