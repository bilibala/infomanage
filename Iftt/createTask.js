import React, {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ToolbarAndroid,
  Image
}
from 'react-native';

import firstpage from '../Device/firstpage';
import selectServices from './selectServices'



class CreateTask extends React.Component {

    constructor(props) {
      super(props);
      this.state = {

      };
    }
    componentDidMount() {


    }


    render() {

      return this._renderLightSence();

    }

    _renderLightSence() {
        return (
            <View style={styles.container}>
                       <ToolbarAndroid
                       navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha')}
                       title = {'创建任务'}
                       style={styles.toolbar}
                       onIconClicked={this.props.navigator.pop}
                       >
                       </ToolbarAndroid>
                       <View style={styles.container}>
                       <View style={styles.textWapper}>
                          <Text style={styles.text}>
                          条件
                          </Text>
                          </View>
                          <View style={styles.imageWapper}>
                           <TouchableOpacity onPress={()=>this._pressTrigger()}>
                          <Image source={require('./image/IFTTT/add.png')}  style={styles.image}/>
                          </TouchableOpacity>
                          </View>
                            <View style={styles.textWapper}>
                          <Text style={styles.text}>
                          执行
                          </Text>
                          </View>
                           <View style={styles.imageWapper}>
                           <TouchableOpacity onPress={()=>this._pressAction()}>
                          <Image source={require('./image/IFTTT/add.png')} style={styles.image}/>
                          </TouchableOpacity>
                          </View>
                        </View>



      </View>
    )

  }

  _pressTrigger(){
      this.props.navigator.push({
        name:'trigger',
        component:selectServices,
        params:{
          navigator:this.props.navigator,
          type:'trigger'
          }
      })
  }

  _pressAction(){
      this.props.navigator.push({
        name:'trigger',
        component:selectServices,
        params:{
          navigator:this.props.navigator,
          type:'action'
          }
      })
  }



}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'

  },
  toolbar: {
    backgroundColor: '#6d9eeb',
    height: 56,
  },
  text: {
    fontSize: 30,
  },
  image: {
    width: 90,
    height: 90,
  },
  imageWapper: {
    flexDirection: 'column',
    alignItems: 'center', 
    justifyContent: 'center',
    marginTop:20
  },
  textWapper: {
    flexDirection: 'column',
    alignItems: 'center',
     justifyContent: 'center',
     marginTop:30
  }});


export default CreateTask