import React, {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ToolbarAndroid,
  Image,
  Switch,
  ScrollView

}
from 'react-native';



class TaskDatail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isopen: false,
      notifyIsopen: false,
    };
  }

  componentDidMount() {

  }

  render() {
    return this._renderScreenSence();
  }

  _renderScreenSence() {
    return (
      <View style={styles.container}>
                       <ToolbarAndroid
                       navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha')}
                       title = {'任务详情'}
                       style={styles.toolbar}
                       onIconClicked={this.props.navigator.pop}
                       >
                       </ToolbarAndroid>
                      <ScrollView contentContainerStyle={styles.content}>
                        <View>
                          <View style={styles.top}>
                            <View style={styles.imageContainer}>
                              <View style={styles.imageLeftWapper}>
                                <Image source={require('./image/IFTTT/weather.png')} style={styles.imageLeft}/>
                              </View>
                              <View style={styles.imageRightWapper}>
                              <Image source={require('./image/IFTTT/weather.png')} style={styles.imageRight}/>
                              </View>
                            </View>
                            <Text style={styles.taskText}>
                            如果天气变成下雨，然后就给你发一封邮件
                            </Text>
                        </View>
                        <View style={styles.bottom}>
                          <View style={styles.taskStatus}>
                            <Text style={styles.createTime}>
                                创建于 2016/4/6
                            </Text>
                            <Text style={styles.useNum}>
                            触发了2次
                            </Text>
                             <Switch style={styles.switch}
                                  disabled={false}
                                  onValueChange={this.onValueChange.bind(this)}
                                  value={this.state.isopen}>
                                  </Switch>
                          </View>
                        </View>
                        <View style={styles.notify}>
                          <View style={styles.notifyIconWrapper}>
                            <Image source={require('./image/IFTTT/notify.png')} style={styles.notifyIcon}>
                            </Image>
                          </View>
                          <Text style={styles.notifyText}>
                            当任务被触发时收到消息提醒
                          </Text>
                          <Switch style={styles.notifySwitch}
                                  disabled={false}
                                  onValueChange={this.notifyOnValueChange.bind(this)}
                                  value={this.state.notifyIsopen}>
                          </Switch>
                        </View>
                        <View style={styles.editButton}>
                          <Text style={styles.editText}>
                            编辑
                          </Text>
                        </View>
                        <View style={styles.delButton}>
                          <Text style={styles.delText}>
                            删除
                          </Text>
                        </View>
                        </View>
                        </ScrollView>
                        </View>
    )

  }

  onValueChange(value) {
    this.setState({
      isopen: value,
    });
  }
  notifyOnValueChange(value) {
    this.setState({
      notifyIsopen: value,
    });
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'

  },
  toolbar: {
    backgroundColor: '#adcaf7',
    height: 56,
  },
  content: {
    flex: 1,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
  },
  top: {
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#adcaf7',
  },
  bottom: {

    flexDirection: 'column',
  },
  imageContainer: {
    width: 230,
    height: 100,
    backgroundColor: '#6d9eeb',
    borderRadius: 10,
    marginTop: 20,
    marginBottom: 20,
    flexDirection: 'row',
  },
  imageLeftWapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageRightWapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  taskText: {
    marginLeft: 40,
    marginRight: 40,
    fontSize: 18,
    color: '#333333',
    fontWeight: 'bold',
    marginBottom: 20,
  },
  imageLeft: {
    width: 80,
    height: 80,
  },
  imageRight: {
    width: 80,
    height: 80,
  },
  taskStatus: {
    padding: 20,
    flexDirection: 'column',
  },
  switch: {
    position: 'absolute',
    top: 20,
    right: 20,
  },
  notify: {
    height: 55,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: '#f2f2f2',
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  notifyIconWrapper:{
    alignItems: 'center',
    justifyContent: 'center',
  },
  notifyIcon:{
    width:25,
    height:25,
    marginLeft:10,
  },
  notifyText:{
    textAlign:'center',
    flex:1,
    marginRight:60,
  },
  notifySwitch:{
    position: 'absolute',
    top: 15,
    right: 0,
  },
  createTime:{
    fontSize:12
  },
  useNum:{
    fontSize:12
  },
  editButton: {
    height: 55,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: '#42c8f4',
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
     marginTop:12,
  }, 
  delButton: {
    height: 55,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: '#ff4401',
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:12,
  },
  editText:{
    flex:1,
    fontSize:20,
    textAlign:'center',
    color:'#fff',
  },
  delText:{
    flex:1,
    fontSize:20,
    textAlign:'center',
    color:'#fff',
  }
});


export default TaskDatail