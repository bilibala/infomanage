'use strict';

import React, {

	Component,
	StyleSheet,

	Text,
	View,
	Image,
	ListView,
	TouchableOpacity,
	Dimensions
}
from 'react-native';


 

 import devicesDetailLight from './devicesDetailLight';
import devicesDetailScreen from './devicesDetailScreen';
import devicesDetailSocket from './devicesDetailSocket';
import Loader from '../component/loader/Loader';


var List_Device =   [
        {
            "id": "11494",
            "title": "好用的插座",
            "year": "备注备注备注备注备注备注备注备注备注被准备阿斯达卡付款哈是否还啊数据库",
            "mpaa_rating": "PG-13",
            "type":"插座",
            "image":"./icon_drawer_head.jpg",
        },
        {
            "id": "11495",
            "title": "美丽的灯",
            "year": "备注备注备注备注备注备注备注备注",
            "mpaa_rating": "PG-13",
            "type":"灯",
            "image":"./icon_drawer_head.jpg",
            
        },
        {
            "id": "11496",
            "title": "巨大的屏幕",
            "year": "备注备注备注备注备注备注备注备注",
            "mpaa_rating": "PG-13",
            "type":"三连屏",
            "image":"./icon_drawer_head.jpg",
        }
        
       

    ];



class DeviceList extends Component{

    constructor(props){
    	super(props);
    	// var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    	  this.state = {
        	dataSource: null,
        	isload:false,
            ds : new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
        }


      


    }

      componentDidMount(){
      	
      
        this.setState({
        	dataSource:this.state.ds.cloneWithRows(List_Device),
        	isload:true 
        });

    


      	 
    }
    

    

      render(){



      	 if (!this.state.isload) {
            return(
            	<View style={{alignItems:'center'}}>
	            	<Loader
	             size={37}
	             isVisible ={true}
                 type={'ThreeBounce'}
	             color={'#6d9eeb'}
	            
	             />
	             </View>
            
        );

      	 }
           else{
           return (
            
             <View style={styles.container}>
	             <ListView
	                dataSource={this.state.dataSource}
	                renderRow={this.renderRow.bind(this)}
	                
	             />
             </View>

             


           	);
          }     


      }
 
  renderRow(deviceInfo,sectionID, rowID){
		
			
    	return(
    	     

    		<TouchableOpacity onPress={()=>this._pressRow(deviceInfo)}>
             <View style ={styles.item}>
                  <Image source={require('./image/icon_drawer_head.jpg')} style={styles.icon} />
                  <View style={styles.middle}>

                  	<Text style={{flex:1,width:150}}>{deviceInfo.title}</Text>
                  	<Text style={{flex:1,width:150}}>{deviceInfo.year}</Text>
                  </View>
                  <View style={styles.right}>
                     <Image source={require('./image/more.png')} style={styles.righticon} />
                  </View>

                
             </View>
        </TouchableOpacity>
    		);
    	
    }

  _pressRow(deviceInfo){
           

  	 const { navigator } = this.props;
  	 if(navigator) {
          if(deviceInfo.type==="灯")
            navigator.push({
                name: 'devicesDetailLight',
                component: devicesDetailLight,
               params:{
                date:deviceInfo,
               }
            })
          if(deviceInfo.type==="插座")
            navigator.push({
                name: 'devicesDetailSocket',
                component: devicesDetailSocket,
               params:{
                date:deviceInfo,
               }
            })
          if(deviceInfo.type==="三连屏")
            navigator.push({
                name: 'devicesDetailScreen',
                component: devicesDetailScreen,
               params:{
                date:deviceInfo,
               }
            })

        }
  }



}


const styles = StyleSheet.create({
 
  container:{
    flex:1,
  },
  item:{
  	flexDirection:'row',
  	marginTop:10,
    marginLeft:10,
    marginRight:10,
    height:80,
     
  },
  icon:{
  	
  	marginTop:20,
  	height:50,
  	width:50,
 	borderRadius:25
  	
  },
  middle:{
  	flex:2,
       flexDirection:'column',
       marginLeft:20,
       marginTop:10,
  },
  
     righticon:{
     	height:20,
     	width:20,
     	transform:[{rotate:'270deg'}],
     	marginTop:30
     },
   spinner:{
     

   }
 
});


export default DeviceList