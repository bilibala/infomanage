'use strict';
import React,{
    View,
    Text,
    StyleSheet,
    ToolbarAndroid,
    TextInput
} from 'react-native';

 




 
 class deviceCreateByID extends React.Component {
       constructor(props) {
         super(props);
         this.state = {
     
       navigator :this.props.navigator,
       ID:""
    
    };
     }


onActionSelected(){
    
    if (this.state.ID.length<10) {
       alert("请输入完整的设备ID");      
    }else{
      
        this.state.navigator.pop();
    }
   
  
}
onChangeText(text){
    
    this.setState({
        ID:text ,
    });

}
     render() {
         return ( 
        <View style={styles.container}>
           <ToolbarAndroid
                       navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha') }
                       title = {'ID添加'}
                        actions={[{title:'完成',show:'always'}]}
                       style={styles.toolbar}
                        onIconClicked={this.state.navigator.pop}
                         onActionSelected={this.onActionSelected.bind(this)}
                         

                       >
                           
                            
                       </ToolbarAndroid>
                       
                          <View style={styles.container}>
                             <TextInput
                               placeholder="请输入设备ID"
                               style={{marginLeft:10,marginRight:10,fontSize:30}}
                               autoFocus ={true}
                               value={this.state.ID}
                               onChangeText={this.onChangeText.bind(this)}
                             />

                          </View>
        </View>
        )
     }
 }
 
 const styles = StyleSheet.create({
   toolbar: {
    backgroundColor: '#6d9eeb',
    height: 56,
  },
  container:{
    flex:1,
    backgroundColor:'#ffffff'
  }
 });
 
 export default deviceCreateByID;
 