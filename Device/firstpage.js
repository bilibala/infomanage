'use strict';
import React, {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  DrawerLayoutAndroid,
  ToolbarAndroid,
  Dimensions,
  ListView,
  ToastAndroid,
  Image,
  BackAndroid

}
from 'react-native';


import DeviceList from './DeviceList';
var DialogAndroid = require('react-native-dialogs');
import Drawer from '../Common/Drawer';
import TabNavigator from 'react-native-tab-navigator';

import deviceCreateByBlueTooth from './deviceCreateByBlueTooth';
import deviceCreateByID from './deviceCreateByID';
import Fileindex from '../File/fileIndex';
import TaskList from '../Iftt/taskList'
import CreateTask from '../Iftt/createTask'
import selectServices from '../Iftt/selectServices'


import GroupView from '../Group/groupView';
import { connect } from 'react-redux';


import {setNavigator} from '../actions/navigatorAction';


var DRAWER_REF = 'drawer';
var DRAWER_WIDTH_LEFT = 56;
var toolbarActions = [
  {
    title: '添加设备',
    show: 'always'
  },
];
var taskToolbarActions = [


  {
    title: '添加任务',
    show: 'always'
  },


];


var _navigator = null;
BackAndroid.addEventListener('hardwareBackPress', function () {
    if (_navigator.getCurrentRoutes().length === 1) {
        return false;
    }
    _navigator.pop();
    return true;

});

import BarcodeScannerpage from './BarcodeScannerpage';


class firstpage extends React.Component {




  Go(id: Number) {
    if (this.props.navigator && id === 2) {
      this.props.navigator.push({
        name: 'BarcodeScannerpage',
        component: BarcodeScannerpage,
        params: {
          navigator: this.props.navigator,
        }
      })
    }
    if (this.props.navigator && id === 0) {
      this.props.navigator.push({
        name: 'deviceCreateByBlueTooth',
        component: deviceCreateByBlueTooth,
        params: {
          navigator: this.props.navigator,
        }
      })
    }
    if (this.props.navigator && id === 1) {
      this.props.navigator.push({
        name: 'deviceCreateByID',
        component: deviceCreateByID,
        params: {
          navigator: this.props.navigator,
        }
      })
    }
    if (this.props.navigator && id === 3) {
      this.props.navigator.push({
        name: 'deviceCreateByBlueTooth',
        component: deviceCreateByBlueTooth,
        params: {
          navigator: this.props.navigator,
        }
      })
    }
  }

  showDialog() {
    var dialog = new DialogAndroid();
    dialog.set({
      "items": [
        "蓝牙添加",
        "ID添加",
        "扫一扫添加",
        "iBeacon"
      ],
      "title": "添加方式选择",
      itemsCallbackSingleChoice: (id, text) => this.Go(id),
    });
    dialog.show();
    }

  constructor(props) {
    super(props);


    this.state = {
      navigator: this.props.navigator,
      selectedTab: 'device'
    }


  }
  componentDidMount(){
    this.props.dispatch(setNavigator(this.props.navigator));
  }


  _navigation() {
        return (
            <Drawer
                navigator={this.props.navigator}
                drawer={()=>this.refs[DRAWER_REF].closeDrawer()}
            />
        );
  }


  render() {

    _navigator = this.props.navigator;
    return (
      <DrawerLayoutAndroid
             ref={DRAWER_REF}
             drawerWidth={Dimensions.get('window').width - DRAWER_WIDTH_LEFT}
             keyboardDismissMode="on-drag"  
             drawerPosition={DrawerLayoutAndroid.positions.Left}
             renderNavigationView={this._navigation.bind(this)}>

           	
          

		   		   <TabNavigator  tabBarStyle={{ height:52}}  >
                <TabNavigator.Item
                  selected={this.state.selectedTab === 'device'}
                  title="设备"
                  renderIcon={() => <Image  style ={styles.tabIcon} source={require('./image/tab_device.png')} />}
                  renderSelectedIcon={() => <Image style ={styles.tabIcon} source={require('./image/tab_device_select.png')} />}
                  
                  onPress={() => this.setState({ selectedTab: 'device' })}>
                  {

                     <View style={styles.container}>
                       <ToolbarAndroid
                       navIcon={require('image!ic_menu_white') }
                       title = {'设备'}
                       actions={toolbarActions}
                       style={styles.toolbar}
                        onIconClicked={() => {this.refs[DRAWER_REF].openDrawer();}}
                        onActionSelected={this.onActionSelected.bind(this)}
                       >      
                            
                       </ToolbarAndroid>
                       
                          <View style={styles.container}>
                           <DeviceList
                              navigator={this.props.navigator}
                            />
                          </View>
                    
                 </View>
                  }
                </TabNavigator.Item>
                <TabNavigator.Item
                  selected={this.state.selectedTab === 'file'}
                  title="文件"
                  renderIcon={() => <Image  style ={styles.tabIcon} source={require('./image/tab_file.png')} />}
                  renderSelectedIcon={() => <Image style ={styles.tabIcon} source={require('./image/tab_file_select.png')} />}
                //  renderBadge={() => <CustomBadgeView />}
                 
                  onPress={() => this.setState({ selectedTab: 'file' })}>
                  {
                         <View style={styles.container}>
 
                              <Fileindex
                               drawer={() => {this.refs[DRAWER_REF].openDrawer();}}
                              navigator={this.props.navigator}/>
                        </View> 
                  }
                </TabNavigator.Item>
                <TabNavigator.Item
                  selected={this.state.selectedTab === 'task'}
                  title="任务"
                  renderIcon={() => <Image  style ={styles.tabIcon} source={require('./image/tab_task.png')} />}
                  renderSelectedIcon={() => <Image style ={styles.tabIcon} source={require('./image/tab_task_select.png')} />}
                //  renderBadge={() => <CustomBadgeView />}
                 
                  onPress={() => this.setState({ selectedTab: 'task' })}>
                  {
                   

                   <View style={styles.container}>
                       <ToolbarAndroid
                       navIcon={require('image!ic_menu_white') }
                       title = {'任务'}
                       actions={taskToolbarActions}
                       style={styles.toolbar}
                        onIconClicked={() => {this.refs[DRAWER_REF].openDrawer();}}
                        onActionSelected={this.onTaskActionSelected.bind(this)}
                       >
                       </ToolbarAndroid>
                       
                          <View style={styles.container}>
                           <TaskList
                              navigator={this.props.navigator}
                            />
                          </View>
                          </View>
                    
                  }
                </TabNavigator.Item>
                <TabNavigator.Item
                  selected={this.state.selectedTab === 'group'}
                  title="群组"
                  renderIcon={() => <Image  style ={styles.tabIcon} source={require('./image/tab_group.png')} />}
                  renderSelectedIcon={() => <Image style ={styles.tabIcon} source={require('./image/tab_group_select.png')} />}
                //  renderBadge={() => <CustomBadgeView />}
                 
                  onPress={() => this.setState({ selectedTab: 'group' })}>
                  {
                            <View style={styles.container}>
                                <GroupView
                                    drawer={() => {this.refs[DRAWER_REF].openDrawer();}}
                                    navigator={_navigator}
                                />
                            </View>

                  }
                </TabNavigator.Item>
              </TabNavigator>  
  </DrawerLayoutAndroid>

    );
  }

  onTaskActionSelected(position) {
    if (position === 0) {
      this.props.navigator.push({
        name: 'task',
        component: CreateTask,
        params: {
          navigator: this.props.navigator
        }
      })
    }
  }

  onActionSelected(position) {
    if (position === 0) {
      this.showDialog();
    }
  }

}
const styles = StyleSheet.create({

  toolbar: {
    backgroundColor: '#6d9eeb',
    height: 56,
  },
  container: {
    flex: 1,
  },
  tabIcon: {
    height: 25,
    width: 25,
  },
  timeLineButton:{
    flex: 1,
  },
  timeLineIcon:{
    height: 25,
    width: 25,
  }

});
export default connect()(firstpage)
