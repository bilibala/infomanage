import React, {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    ToolbarAndroid,
    Image,
    Switch

} from 'react-native';

import firstpage from './firstpage';
import screenControlAction from '../actions/screenControlAction';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux'

var toolbarActions =[


  {title: '删除设备', show: 'never'},
   {title: '分享', show: 'never'},


];


class devicesDetailScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          date:this.props.date,
          isopen:false,
        };
    }
   componentDidMount() {
        //这里获取从FirstPageComponent传递过来的参数: id

    }
    _pressButton() {
        const { navigator } = this.props;
        if(navigator) {
            //很熟悉吧，入栈出栈~ 把当前的页面pop掉，这里就返回到了上一个页面:FirstPageComponent了
            navigator.pop();
        }

    }

    render() {

          return this._renderScreenSence();


    }

     _renderScreenSence(){
        return(
            <View style={styles.container}>
                       <ToolbarAndroid
                       navIcon={require('image!abc_ic_ab_back_mtrl_am_alpha')}
                       title = {'设备详情'}
                        actions={toolbarActions}
                       style={styles.toolbar}
                       onIconClicked={this.props.navigator.pop}
                        onActionSelected={this.onActionSelected}
                       >


                       </ToolbarAndroid>

                       <View style={styles.content}>

                            <View  style={styles.info}>
                                 <Image
                                   style={styles.infoIcon}
                                   source={require('./image/icon_drawer_head.jpg')} />
                                 <View style={styles.infoText}>
                                 <Text style={styles.infoText1}>设备名称：{this.state.date.title}</Text>
                                 <Text style={styles.infoText2}>添加方式：{this.state.date.mpaa_rating}</Text>
                                 </View>
                            </View>

                            <View  style={styles.space}>

                            </View>

                            <View style={styles.controll}>
                                  <Text style={{alignSelf :'center',flex:3,marginLeft:30}}>设备开关</Text>
                                  <Switch  style={{flex:1,marginRight:30}}
                                  disabled={false}
                                  onValueChange={this.onValueChange.bind(this)}
                                  value={this.state.isopen}

                                  ></Switch>
                            </View>

                            <View  style={styles.space}>
                            </View>

                            <View style={styles.controll}>
                                  <Text style={{alignSelf :'center',flex:3,marginLeft:30}}>添加播放文件</Text>
                               <Image source={require('./image/more.png')} style={{marginRight:30,height:20,width:20,marginTop:20, transform:[{rotate:'270deg'}]}} />

                            </View>
                            <View style={styles.controll}>
                                  <Text style={{alignSelf :'center',flex:3,marginLeft:30}}>播放文件</Text>
                                <Image source={require('./image/more.png')} style={{marginRight:30,height:20,width:20,marginTop:20,transform:[{rotate:'270deg'}]}} />


                            </View>

                            <View  style={styles.space}>
                            </View>

                            <View  style={{flexDirection:'column',marginTop:10}}>
                              <Text> 2015/08/07 11:11 飞飞打开设备 </Text>
                               <Text> 2015/08/07 11:11 飞飞打开设备 </Text>
                                <Text> 2015/08/07 11:11 飞飞打开设备 </Text>
                            </View>

                       </View>

                 </View>
          )

     }


    onActionSelected(position){
        if(position===0)
          alert('删除设备');
        if(position===1)
          alert('分享设备');
  }
  onValueChange(value){
     const{ScreenAction} = this.props;


     this.setState({
       isopen:value,
     });
    if (value) {
        //调用打开动作
        ScreenAction.openWebWithScreen();

    }else {
      //调用关闭动作
        ScreenAction.closeScreen();
    }


  }

}
const styles = StyleSheet.create({
     container:{
         flex:1,
          backgroundColor:'#ffffff'

     },
     content:{
    flex:1,
    flexDirection:'column'

  },
  toolbar: {
    backgroundColor: '#6d9eeb',
    height: 56,
  },
  info:{

     flexDirection:'row',
     height:120,
   //  backgroundColor:'#fce5cd'
  },
  infoIcon:{
    height:60,
    width:60,
    alignSelf:'center',
    marginLeft:60
  },
  infoText:{
        alignSelf:'center',
        marginLeft:20,
        flexDirection:'column'
  },
  infoText1:{

        marginLeft:20
  },
  infoText2:{

        marginLeft:20
  },
  space:{
    backgroundColor:'#cccccc',
    height:15
  },
  controll:{
    flexDirection:'row',
    height:60

  }
});


export default connect(state=>{  return {
  isopen:state.screenControl.isopen

 }} ,
  dispatch => {
  return {
     ScreenAction: bindActionCreators(screenControlAction, dispatch),
     }}
)(devicesDetailScreen)
